package com.example.bestgameever;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.bestgameever.DataBaseHelper.DatabaseHelper;
import com.example.bestgameever.DataBaseHelper.Calculation;
import com.example.bestgameever.calculations.CalcClass;

import java.util.ArrayList;

public class SumActivity extends Activity {

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(this);
        setContentView(R.layout.activity_sum);
    }

    public void calcClick(View v)
    {
        TextView from = (TextView)findViewById(R.id.editText);
        TextView to = (TextView)findViewById(R.id.editText2);
        TextView result = (TextView)findViewById(R.id.editText3);

        CalcClass calculations = new CalcClass();
        int Sum = calculations.calcSum(Integer.parseInt(from.getText().toString()), Integer.parseInt(to.getText().toString()));
        databaseHelper.addCalculation(Integer.parseInt(from.getText().toString()), Integer.parseInt(to.getText().toString())," Intervalo suma ", Integer.toString(Sum));
        result.setText(Integer.toString(Sum));

        ArrayList<Calculation> calculations1 = databaseHelper.getAllCalculations();

        for (Calculation calculation : calculations1){
            Log.d("Result: ", "Pirmas elementas: " + calculation.first + " " + "Antras elementas: " + calculation.second + calculation.action + calculation.result);
        }
    }

    public void mainClick(View v)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
