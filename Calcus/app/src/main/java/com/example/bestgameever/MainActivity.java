package com.example.bestgameever;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bestgameever.DataBaseHelper.Calculation;
import com.example.bestgameever.DataBaseHelper.DatabaseHelper;
import com.example.bestgameever.calculations.CalcClass;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcClick(View v)
    {
        Intent intent = new Intent(this, CalculationActivity.class);
        startActivity(intent);
    }

    public void profClick(View v)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    public void aboutClick(View v)
    {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
}
