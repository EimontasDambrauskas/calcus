package com.example.bestgameever;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.icu.util.IslamicCalendar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bestgameever.DataBaseHelper.Calculation;
import com.example.bestgameever.DataBaseHelper.DatabaseHelper;
import com.example.bestgameever.calculations.CalcClass;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class FibonacciActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(this);
        setContentView(R.layout.activity_fibonacci);
    }

    public void calcClick(View v)
    {
        TextView to = (TextView)findViewById(R.id.editText2);
        TextView result = (TextView)findViewById(R.id.editText3);

        CalcClass calculations = new CalcClass();
        int Fib = calculations.fibonacci(Integer.parseInt(to.getText().toString()));
        databaseHelper.addCalculation(0, Integer.parseInt(to.getText().toString()), " n-tojo fibonačio skaičiaus radimas", Integer.toString(Fib));
        result.setText(Integer.toString(Fib));

        ArrayList<Calculation> calculations1 = databaseHelper.getAllCalculations();

        for (Calculation calculation : calculations1){
            Log.d("Result: ", calculation.first + "Pasirinktas n-tasis skaičius: " + calculation.second + " " + calculation.action + " Rezultatas: " + calculation.result);
        }
    }

    public void mainClick(View v)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
