package com.example.bestgameever.DataBaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "calculation_database";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_CALCULATIONS = "calculations";
    private static final String KEY_ID = "id";
    private static final String KEY_FIRST = "first_number";
    private static final String KEY_SECOND = "second_number";
    private static final String KEY_ACTION = "action";
    private static final String KEY_RESULT = "result";

    /*CREATE TABLE students ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, phone_number TEXT......);*/

    private static final String CREATE_TABLE_CALCULATIONS = "CREATE TABLE "
            + TABLE_CALCULATIONS + "(" + KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_FIRST + " INT," + KEY_SECOND + " INT," + KEY_ACTION + " TEXT," + KEY_RESULT + " TEXT" + ");";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CALCULATIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_CALCULATIONS + "'");
        onCreate(db);
    }

    public long addCalculation(Integer first_number, Integer second_number, String action, String result) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Creating content values
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST, first_number);
        values.put(KEY_SECOND, second_number);
        values.put(KEY_ACTION, action);
        values.put(KEY_RESULT, result);
        // insert row in students table
        long insert = db.insert(TABLE_CALCULATIONS, null, values);

        return insert;
    }

    public ArrayList<Calculation> getAllCalculations() {
        ArrayList<Calculation> calculationsArrayList = new ArrayList<Calculation>();
        String action, result = "";
        Integer first, second;
        String selectQuery = "SELECT  * FROM " + TABLE_CALCULATIONS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                first = c.getInt(c.getColumnIndex(KEY_FIRST));
                second = c.getInt(c.getColumnIndex(KEY_SECOND));
                action = c.getString(c.getColumnIndex(KEY_ACTION));
                result = c.getString(c.getColumnIndex(KEY_RESULT));
                // adding to Students list
                calculationsArrayList.add(new Calculation(first, second, action, result));
            } while (c.moveToNext());
        }
        return calculationsArrayList;
    }
}
