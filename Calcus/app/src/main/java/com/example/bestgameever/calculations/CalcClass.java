package com.example.bestgameever.calculations;

import java.util.ArrayList;

public class CalcClass {

    public int calcSum(int arg1, int arg2)
    {
        int sum = 0;

        for(int i = arg1; i <= arg2; i++)
        {
            sum += i;
        }
        return sum;
    }

    public ArrayList<String> getEvenNumber(int arg1, int arg2)
    {
        ArrayList<String> list = new ArrayList<>();
        for(Integer i = arg1; i <= arg2; i++)
        {
            if(i % 2 == 0) {
                list.add(i.toString());
            }
        }
        return list;
    }

    public ArrayList<String> getOddNumber(int arg1, int arg2)
    {
        ArrayList<String> list = new ArrayList<>();
        for(Integer i = arg1; i <= arg2; i++)
        {
            if(i % 2 != 0) {
                list.add(i.toString());
            }
        }
        return list;
    }

    public int fibonacci(int x){
        int[] sequence = new int[x];
        sequence[0] = 1;
        sequence[1] = 1;
        if (x > 1){
            for (int i = 2; i < x; i++){
                sequence[i] = sequence[i-1] + sequence[i-2];
            }
        }
        return sequence[x-1];
    }
}
