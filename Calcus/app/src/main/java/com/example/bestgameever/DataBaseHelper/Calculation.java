package com.example.bestgameever.DataBaseHelper;

public class Calculation {
    public Integer first, second;
    public String action, result;

    public Calculation(Integer first, Integer second, String action, String result)
    {
        this.first = first;
        this.second = second;
        this.action = action;
        this.result = result;
    }
}
