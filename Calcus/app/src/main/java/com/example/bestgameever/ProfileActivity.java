package com.example.bestgameever;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.bestgameever.DataBaseHelper.Calculation;
import com.example.bestgameever.DataBaseHelper.DatabaseHelper;

import java.util.ArrayList;

public class ProfileActivity extends Activity {

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(this);
        setContentView(R.layout.activity_profile);

        ArrayList<String> listas = new ArrayList<String>();
        ArrayList<Calculation> calculations1 = databaseHelper.getAllCalculations();

        for (Calculation calculation : calculations1){
            String eilute = "";
            eilute +=  "Intervalo pradžia: " + calculation.first+ " Intervalo pabaiga: " + calculation.second + " " + calculation.action + " Rezultatas: " + calculation.result;
            listas.add(eilute);
        }

        ListView listView = (ListView) findViewById(R.id.listas);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, listas);
        listView.setAdapter(adapter);
    }

    public void mainClick(View v)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
