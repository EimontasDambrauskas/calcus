package com.example.bestgameever;

import android.app.Activity;
import android.content.Intent;
import android.icu.util.IslamicCalendar;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bestgameever.DataBaseHelper.Calculation;
import com.example.bestgameever.DataBaseHelper.DatabaseHelper;
import com.example.bestgameever.calculations.CalcClass;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class EvenNumbersActivity extends Activity {

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(this);
        setContentView(R.layout.activity_even_numbers);
    }

    public void calcClick(View v)
    {
        TextView from = (TextView)findViewById(R.id.editText);
        TextView to = (TextView)findViewById(R.id.editText2);

        CalcClass calculations = new CalcClass();
        ArrayList<String> listas = calculations.getEvenNumber(Integer.parseInt(from.getText().toString()), Integer.parseInt(to.getText().toString()));

        ListView listView = (ListView) findViewById(R.id.listas);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, listas);

        listView.setAdapter(adapter);
        String eilute = "";

        for (String element:listas)
        {
            eilute += element + " ";
        }

        databaseHelper.addCalculation(Integer.parseInt(from.getText().toString()), Integer.parseInt(to.getText().toString()),"Lyginių skaičių radimas intervale.", eilute);
        ArrayList<Calculation> calculations1 = databaseHelper.getAllCalculations();

        for (Calculation calculation : calculations1){
            Log.d("Result: ", calculation.first + calculation.second + calculation.action + calculation.result);
        }

    }

    public void mainClick(View v)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
